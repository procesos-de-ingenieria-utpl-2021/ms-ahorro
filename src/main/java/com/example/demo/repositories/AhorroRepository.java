package com.example.demo.repositories;

import java.util.List;

import com.example.demo.model.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AhorroRepository extends JpaRepository<ahorro, Long>{
    @Query(
        value = "SELECT * FROM ahorro i WHERE i.user_id = ?1",
        nativeQuery = true
    )
    List<ahorro> findByUserId(long userId);
}

