package com.example.demo.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "ahorro")
public class ahorro {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "TAhorro")
    private String tipoAhorro;

    @Column(name = "nomper")
    private String nomprepersona;

    @Column(name = "sald")
    private Double saldoAhorro;

    @Column(name = "user_id")
    private long userId;

    public ahorro(String tipoAhorro, String nomprepersona, Double saldoAhorro, long userId) {
        this.tipoAhorro = tipoAhorro;
        this.nomprepersona = nomprepersona;
        this.saldoAhorro = saldoAhorro;
        this.userId = userId;
    }

    
}

