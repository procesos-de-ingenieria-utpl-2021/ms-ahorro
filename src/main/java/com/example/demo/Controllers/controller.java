package com.example.demo.Controllers;

import java.util.List;

import com.example.demo.repositories.AhorroRepository;
import com.example.demo.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/demo")
public class controller {

    @Autowired
    AhorroRepository ahorroRepository;


    @GetMapping("/ahorro")
    public ResponseEntity<List<ahorro>> findAhorroByUserId(@RequestParam(required = true) long id) {
        List<ahorro> ahorro = ahorroRepository.findByUserId(id);
        return new ResponseEntity<>(ahorro, HttpStatus.OK);
    }

    @PostMapping("/ahorro")
    public ResponseEntity<ahorro> registerAhorro(@RequestBody ahorro ahorro) {
        try {
            ahorro created = ahorroRepository.save(
                    new ahorro(ahorro.getTipoAhorro(), ahorro.getNomprepersona(), ahorro.getSaldoAhorro(), ahorro.getUserId()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

